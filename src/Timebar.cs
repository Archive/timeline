//
// Timebar.cs
//
// Copyright (C) 2004 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;

namespace Timeline {

	public class Timebar : Gtk.DrawingArea {

		public delegate void ClickedHandler (Timebar bar, DateTime when, Activity act);
		public event ClickedHandler ClickedEvent;

		public delegate void MotionHandler (Timebar bar, DateTime when, Activity act);
		public event MotionHandler MotionEvent;

		private struct Region {
			public Activity  Activity;
			public double    Start;
			public double    End;
			public Gdk.Color Color;
		}

		private Journal journal;
		private ArrayList regions = new ArrayList ();
		private double min = 0, max = -1;
		private DateTime minTime, maxTime;

		public Timebar (Journal journal)
		{
			SetSizeRequest (-1, 40);

			this.journal = journal;

			this.Events |= Gdk.EventMask.ButtonPressMask;
			this.Events |= Gdk.EventMask.PointerMotionMask;

			this.ExposeEvent += OnExposed;
			this.ButtonPressEvent += OnButtonPress;
			this.MotionNotifyEvent += OnMotionNotify;

			foreach (Activity activity in this.journal.Activities)
				AddActivity (activity);
		}

		void OnExposed (object o, Gtk.ExposeEventArgs args)
		{
			Paint ();
		}
 
		void OnButtonPress (object o, Gtk.ButtonPressEventArgs args)
		{	
			DateTime dt = PositionToTime (args.Event.X);
			Activity act = PositionToActivity (args.Event.X);
			
			if (ClickedEvent != null)
				ClickedEvent (this, dt, act);
		}

		void OnMotionNotify (object o, Gtk.MotionNotifyEventArgs args)
		{
			if ((args.Event.State & Gdk.ModifierType.Button1Mask) == Gdk.ModifierType.Button1Mask
				&& MotionEvent != null) {
				DateTime dt = PositionToTime (args.Event.X);
				Activity act = PositionToActivity (args.Event.X);
			
				MotionEvent (this, dt, act);
			}
		}

		//////////////////////////////////

		public void AddActivity (Activity activity)
		{
			Gdk.Colormap colormap = Gdk.Colormap.System;

			double start = activity.StartTime.Ticks;
			double end   = activity.EndTime.Ticks;

			Region region = new Region ();
			
			region.Activity = activity;
			region.Start    = start;
			region.End      = end;

			region.Color = new Gdk.Color (activity.ColorRedByte,
						      activity.ColorGreenByte,
						      activity.ColorBlueByte);
			colormap.AllocColor (ref region.Color, true, true);

			if (min > max) {
				min = start;
				max = end;
				minTime = activity.StartTime;
				maxTime = activity.EndTime;
			} else {
				if (start < min) {
					min = start;
					minTime = activity.StartTime;
				}
				if (end > max) {
					max = end;
					maxTime = activity.EndTime;
				}
			}

			regions.Add (region);
		}

		private void Paint ()
		{
			Gdk.Drawable drawable = this.GdkWindow;
			Gdk.GC gc = new Gdk.GC (drawable);

			Gdk.Rectangle rect = this.Allocation;
			double w = rect.Width;

			foreach (Region region in regions) {
				gc.RgbBgColor = region.Color;
				gc.RgbFgColor = region.Color;

				double x0 = w * (region.Start - min) / (max - min);
				double x1 = w * (region.End - min) / (max - min);

				drawable.DrawRectangle (gc, true,
							(int) x0, 0,
							(int) (x1-x0+1), rect.Height);
			}

			DateTime hr = new DateTime (minTime.Year,
						    minTime.Month,
						    minTime.Day,
						    minTime.Hour,
						    0, 0);

			Gdk.Color tickColor = new Gdk.Color (0, 0, 0);
			Gdk.Colormap.System.AllocColor (ref tickColor, true, true);
			gc.RgbFgColor = tickColor;

			Pango.Layout layout = new Pango.Layout (this.PangoContext);
			layout.FontDescription = Pango.FontDescription.FromString ("Sans 7");

			while (hr <= maxTime) {
				int h = hr.Hour;
				int m = hr.Minute;
				layout.SetMarkup (String.Format ("{0}:{1:00}", h, m));

				int t = (int) (w * (hr.Ticks - min) / (max - min));
				drawable.DrawLine (gc, t, 0, t, rect.Height / 4);

				Pango.Rectangle ink = new Pango.Rectangle ();
				Pango.Rectangle logical = new Pango.Rectangle ();
				layout.GetPixelExtents (out ink, out logical);

				this.GdkWindow.DrawLayout (this.Style.TextGC (Gtk.StateType.Normal), t - ink.Width / 2, rect.Height / 4 + ink.Width / 10, layout);

				hr = hr.AddMinutes (30.0);
			}
		}

		private DateTime PositionToTime (double pos)
		{
			double t = min + (max - min) * (pos / this.Allocation.Width);
			return new DateTime ((long) t);
		}

		private Activity PositionToActivity (double pos)
		{
			DateTime dt = PositionToTime (pos);
			return journal.GetActivityByTime (dt);
		}
	}
}
