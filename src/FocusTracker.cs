using System;
using System.Collections;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Gnome;


namespace Timeline {

	public class FocusSample {
		
		public string WinTitle;
		public string GroupClass;
		public string GroupName;
		public string XID
;
		public DateTime SampleTime;
		public bool ScreensaverRunning;

		private IntPtr wnck_win;
		private IntPtr wnck_class_group;

		public FocusSample (IntPtr win)
		{
			wnck_win = win;

			this.SampleTime = System.DateTime.Now;
			this.WinTitle = Marshal.PtrToStringAnsi (wnck_window_get_name (win));

			IntPtr wnck_class_group = wnck_window_get_class_group (win);
			if (wnck_class_group != IntPtr.Zero) {
 				this.GroupClass = Marshal.PtrToStringAnsi (wnck_class_group_get_res_class (wnck_class_group));
				this.GroupName = Marshal.PtrToStringAnsi (wnck_class_group_get_name (wnck_class_group));
			}

			this.XID = wnck_window_get_xid (win).ToString ();
		}

		public Gdk.Pixbuf WindowIcon {
			get {
				return new Gdk.Pixbuf (wnck_window_get_icon (wnck_win));
			}
		}

		public Gdk.Pixbuf ClassIcon {
			get {
				return new Gdk.Pixbuf (wnck_class_group_get_icon (wnck_class_group));
			}
		}

		[DllImport("libwnck-1")]
			static extern ulong wnck_window_get_xid (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_window_get_icon (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_window_get_name (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_window_get_class_group (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_class_group_get_res_class (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_class_group_get_name (IntPtr raw);

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_class_group_get_icon (IntPtr raw);

	}

	public class FocusTracker : IDisposable {
		public delegate void FocusChangedHandler (FocusTracker source, FocusSample sample);
		public event FocusChangedHandler FocusChanged;

		delegate void ActiveWindowChangedCallback ();

		IntPtr screen;
		uint window_signal_id;
		uint workspace_signal_id;
		ActiveWindowChangedCallback window_changed_callback;

		private void ActiveWindowChangedCb () {
			
			IntPtr active_win = wnck_screen_get_active_window (screen);
			if (active_win == IntPtr.Zero)
				return;

			FocusSample sample = new FocusSample (active_win);

			FocusChanged (this, sample);
		}

		public FocusTracker () {
			screen = wnck_screen_get_default ();

			window_changed_callback = 
				new ActiveWindowChangedCallback (ActiveWindowChangedCb);

			window_signal_id =
				g_signal_connect_data (screen,
						       "active_window_changed",
						       window_changed_callback,
						       IntPtr.Zero,
						       IntPtr.Zero,
						       0);

			workspace_signal_id =
				g_signal_connect_data (screen,
						       "active_workspace_changed",
						       window_changed_callback,
						       IntPtr.Zero,
						       IntPtr.Zero,
							   0);
		}

		public void Dispose ()
		{
			g_signal_handler_disconnect (screen, window_signal_id);
			g_signal_handler_disconnect (screen, workspace_signal_id);
		}
		
		[DllImport("libwnck-1")]
			static extern IntPtr wnck_screen_get_default ();

		[DllImport("libwnck-1")]
			static extern IntPtr wnck_screen_get_active_window(IntPtr raw);

		[DllImport("libgobject-2.0.dll")]
			static extern uint g_signal_connect_data(IntPtr       obj, 
								 String       name, 
								 Delegate     cb, 
								 IntPtr       user_data, 
								 IntPtr       destroy_cb, 
								 int          flags);

		[DllImport("libgobject-2.0.dll")]
			static extern void g_signal_handler_disconnect (IntPtr instance, uint handler);
	}
}

