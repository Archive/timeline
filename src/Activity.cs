//
// Activity.cs
//
// Copyright (C) 2004 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;
using System.IO;

namespace Timeline {

	public class Activity {

		private DateTime start;
		private DateTime end;

		private int elapsed;
		private string application;
		private string title;
		private string id;
		private string screenshot;
		private bool screensaver_status;

		public Activity (FocusSample start_sample, FocusSample end_sample)
		{
			if (start_sample.ScreensaverRunning) {
				application = "Screen Saver";
				title = "Screen Saver";
				id = "screensaver";
			} else {
				application = start_sample.GroupClass;
				title = start_sample.WinTitle;
				id = start_sample.XID;
			}

			start = start_sample.SampleTime;
			end = end_sample.SampleTime;
			elapsed = (int) (end_sample.SampleTime - start_sample.SampleTime).TotalSeconds;
			screensaver_status = start_sample.ScreensaverRunning;
		}

		public Activity (string csvLine)
		{
			char[] seps = { ',' };
			string[] parts = csvLine.Split (seps);

			application = parts [0].Replace ('\"', ' ');
			title = parts [1];

			try {
				elapsed = int.Parse (parts [3]);
			} catch {
				Console.WriteLine (csvLine);
				elapsed = 0;
			}
			
			start = new DateTime (1970, 1, 1).ToLocalTime ();

			int seconds;
			try {
				seconds = int.Parse (parts [4]);
			} catch {
				seconds = 0;
			}
			start = start.AddSeconds (seconds);

			end = new DateTime (1970, 1, 1).ToLocalTime ();
			end = end.AddSeconds (int.Parse (parts [5]));

			screenshot  = parts [6];
			id = parts [7];

			if (parts [2] == "active")
				screensaver_status = false;
			else
				screensaver_status = true;
		}

		public DateTime StartTime {
			get { return start; }
		}

		public DateTime EndTime {
			get { return end; }
		}

		public bool Contains (DateTime dt)
		{
			return StartTime <= dt && dt < EndTime;
		}

		public int ElapsedTime {
			get { return elapsed; }
		}

		public string Application {
			get { return application; }
		}

		public string Title {
			get { return title; }
		}
		
		public string Id {
			get { return id; }
		}

		public string Screenshot {
			get { 
				if (String.IsNullOrEmpty (screenshot))
					return null;

				string path = Environment.GetEnvironmentVariable ("HOME");
				path = Path.Combine (path, ".timeline");
				path = Path.Combine (path, screenshot);
				return path;
			}
		}

		public bool ScreensaverStatus {
			get { return screensaver_status; }
		}

		private byte StringToByte (string str, uint magic)
		{
			uint hash = 1;
			foreach (char c in str)
				hash = hash * magic + (uint) c;
			return (byte) (hash % 256);
		}

		public byte ColorRedByte {
			get { return StringToByte (Application, 3); }
		}

		public byte ColorGreenByte {
			get { return StringToByte (Application, 47); }
		}

		public byte ColorBlueByte {
			get { return StringToByte (Application, 37); }
		}
	}
}
