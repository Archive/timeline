//
// TimelineWindow.cs
//
// Copyright (C) 2004 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;

namespace Timeline {

	class TimelineWindow : Gtk.Window {

		static int refs = 0;

		static public void IncRef ()
		{
			++refs;
		}

		static public void DecRef ()
		{
			--refs;
			if (refs <= 0)
				Gtk.Application.Quit ();
		}

		Journal        journal;
		Gtk.VBox       main;
		Timebar        bar;
		Gtk.Image      image;
		//Gtk.HTML       html;
		//Gtk.HTMLStream stream;
		Gtk.Label      html;
		Gtk.Label      label;
		Activity       current_activity;

		public TimelineWindow (Journal journal) : base (Gtk.WindowType.Toplevel)
		{
			IncRef ();

			this.Title         = "Timeline";
			this.DefaultWidth  = 750;
			this.DefaultHeight = 600;

			this.journal = journal;

			DeleteEvent += new Gtk.DeleteEventHandler (this.DoDelete);

			JournalWatcher watcher = new JournalWatcher (journal, 10 /* seconds */);
			watcher.ChangeEvent += OnJournalChanged;

			main = new Gtk.VBox (false, 3);

			AddTimebar ();
			
			Gtk.HBox hbox = new Gtk.HBox (false, 3);
			main.PackStart (hbox, false, true, 3);

			image = new Gtk.Image ();
			hbox.PackStart (image, true, true, 3);
			image.Show ();

			label = new Gtk.Label ();
			label.Show ();
			hbox.PackStart (label, true, true, 3);

			hbox.Show ();

			current_activity = journal.FirstActivity ();
			UpdateCurrentActivity ();

			/*html = new Gtk.HTML ();
			view.Add (this.html);*/
			html = new Gtk.Label ();

			Gtk.ScrolledWindow view = new Gtk.ScrolledWindow ();
			view.SetPolicy (Gtk.PolicyType.Never, Gtk.PolicyType.Automatic);
			view.Add (html);

			html.Show ();
			view.Show ();
			main.PackStart (view, true, true, 0);

			UpdateSummary ();

			Add (main);
			main.Show ();
		}

		private void UpdateCurrentActivity ()
		{
			if (current_activity == null)
				return;

			AppTime at = new AppTime ();
			at.ElapsedTime = current_activity.ElapsedTime;

			if (current_activity.Screenshot != null)
				image.FromFile = current_activity.Screenshot;

			if (current_activity.Application != null) {

				string app;
				app = current_activity.Application;
				if (app == "<unknown>")
					app = "<i>Unknown</i>";
				
				label.Markup = String.Format ("<b>{0}</b>\n\nFrom {1}:{2:00} to {3}:{4:00}\n{5}",
							      app,
							      current_activity.StartTime.Hour,
							      current_activity.StartTime.Minute,
							      current_activity.EndTime.Hour,
							      current_activity.EndTime.Minute,
							      at.PrettyTime ());
			}
		}

		private void AddTimebar () 
		{
			this.bar = new Timebar (journal);
			this.bar.MotionEvent += OnMotion;
			this.bar.ClickedEvent += OnClicked;
			this.bar.Show ();

			main.PackStart (this.bar, false, true, 3);
			main.ReorderChild (this.bar, 0);
		}

		private void UpdateSummary ()
		{
			string summary = journal.GenerateSummary ();
			//this.stream = this.html.Begin ();
			//this.stream.Write ("<html><body>" + summary + "</body></html>");
			this.html.Text = summary;
		}

		private void OnJournalChanged (JournalWatcher watcher, Journal journal) 
		{
			UpdateSummary ();

			this.main.Remove (this.bar);
			AddTimebar ();
		}

		private void OnClicked (Timebar bar, DateTime when, Activity activity)
		{
			this.current_activity = activity;
			UpdateCurrentActivity ();
		}

		private void OnMotion (Timebar bar, DateTime when, Activity activity)
		{
			this.current_activity = activity;
			UpdateCurrentActivity ();
		}

		private void DoDelete (object o, Gtk.DeleteEventArgs args)
		{
			Close ();
		}

		private void Close ()
		{
			DecRef ();
			Destroy ();
		}

	}
}
