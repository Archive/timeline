using System;
using System.Diagnostics;

namespace Timeline {
	public class ActivitySample
	{
		public string   WinTitle;
		public string   WinClass;
		public string   WinID;
		public DateTime SampleTime;
		public bool     ScreensaverRunning;
			
		public ActivitySample ()
		{
			this.Update ();
		}

		public void Update ()
		{
			SampleTime = System.DateTime.Now;

			// Get information about the currently focused window
			Process xgetfocus = new Process ();
			xgetfocus.StartInfo.FileName               = "xgetfocus";
			xgetfocus.StartInfo.UseShellExecute        = false;
			xgetfocus.StartInfo.RedirectStandardOutput = true;

			try {
				xgetfocus.Start ();
			} catch {
				Console.WriteLine ("Exception checking focus...");

				return;
			}
			

			string line;
			
			while ( (line = xgetfocus.StandardOutput.ReadLine ()) != null) {

				if (line.StartsWith ("NAME=")) {
					WinTitle = line.Substring (6, line.Length - 7);
				}

				if (line.StartsWith ("CLASSHINT_RES_NAME=")) {
					WinClass = line.Substring (20, line.Length - 21);
				}

				if (line.StartsWith ("WID=")) {
					WinID = line.Substring (4, line.Length - 5);
				}
			}

			xgetfocus.Close ();

			// Find out whether the screensaver is active
			Process xscreensaver = new Process ();
			xscreensaver.StartInfo.FileName               = "xscreensaver-command";
			xscreensaver.StartInfo.Arguments              = "-time";
			xscreensaver.StartInfo.UseShellExecute        = false;
			xscreensaver.StartInfo.RedirectStandardOutput = true;

			try {
				xscreensaver.Start ();
			} catch {
				Console.WriteLine ("Exception checking screensaver state...");

				ScreensaverRunning = false;
				return;
			}

			line = xscreensaver.StandardOutput.ReadLine ();
			if (line == null)
				ScreensaverRunning = false;
			else if (line.IndexOf ("screen blanked") != -1)
				ScreensaverRunning = true;

			xscreensaver.Close ();
		}

		public override bool Equals (object obj) {
			ActivitySample that = (ActivitySample) obj;

			return this.WinTitle == that.WinTitle &&
				this.WinClass == that.WinClass &&
				this.ScreensaverRunning == that.ScreensaverRunning;
		}
		
	}
}
