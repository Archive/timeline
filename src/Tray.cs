//
// Tray.cs
//
// Copyright 2004 Novell, Inc.
//

using System;
using System.Collections;

using Gtk;
using Gdk;
using Gnome;
/*using GtkSharp;
using GLib;*/

namespace Timeline {

	public class TrayApplet	{

		static FocusTracker tracker;
		static FocusSample last_sample;
		static Journal journal;

		static bool show_time = true;
		static int pie_width = 400;
		static int pie_height = 400;

		static Gtk.Window piewin;
		static Gtk.ToggleButton button;
		static NotificationArea icon;
		static PieChart chart;

		private static void HandleFocusChanged (FocusTracker ft, FocusSample sample) 
		{
			// Clean it up, and ignore it if necessary
			if (! FilterSample (sample))
				return;

			if (last_sample == null) {
				last_sample = sample;
				return;
			}

			// Create a record of what happened during the last sample interval
			// Activity activity = new Activity (last_sample, sample);

			// Time since the previous sample
			int sample_elapsed = (int) (sample.SampleTime - last_sample.SampleTime).TotalSeconds;

			// Update all the applications with their
			// running-time, based on this sample data.
			journal.UpdateAppTimes (last_sample.GroupClass, sample_elapsed);

			Activity interappactivity = new Activity (last_sample, sample);
			journal.AddActivity (interappactivity);

			AppTime at = (AppTime) journal.AppTimeTable [sample.GroupClass];

			UpdateLabel (sample, at);

			if (piewin != null) {
				PopulatePieChart ();
				PositionPieWin ();
			}

			last_sample = sample;

			return;
		}

		private static string PrettyTime (int totalsecs)
		{
			string text = "";
			int hours, minutes, seconds;

			hours   = totalsecs / 3600;
			minutes = (totalsecs / 60) % 60;
			seconds = totalsecs % 60;

			if (hours > 0)
				text = String.Format (" ({0}:{1:00}:{2:00})", hours, minutes, seconds);
			else if (minutes > 0)
				text = String.Format (" ({0} mins)", minutes);

			return text;
		}

		private static void UpdateLabel (FocusSample sample, AppTime at)
		{
		        button.Label = sample.GroupClass;

			if (at == null)
				return;

			if (show_time) {
				button.Label += PrettyTime (at.ElapsedTime);
			} else {
				double percentage = 100.0 * (double) at.ElapsedTime / (double) journal.TotalElapsed;

				if (journal.TotalElapsed > 0)
					button.Label += " (" + (int) percentage + "%)";
			}
		}

		private static bool FilterSample (FocusSample sample)
		{

			if (sample.GroupClass == null)
				return false;

			sample.GroupClass =
				System.Char.ToUpper (sample.GroupClass.ToCharArray (0, 1) [0]) +
				sample.GroupClass.Substring (1);

			// Don't count the panel 
			if (sample.GroupClass == "Gnome-panel")
				return false;

			sample.GroupClass = sample.GroupClass.Replace ("-bin", "");
			sample.GroupClass = sample.GroupClass.Replace ("Nautilus", "File Manager");
			sample.GroupClass = sample.GroupClass.Replace ("Gaim", "Instant Messenger");
			sample.GroupClass = sample.GroupClass.Replace ("Pidgin", "Instant Messenger");
			sample.GroupClass = sample.GroupClass.Replace ("Epiphany", "Web Browser");
			sample.GroupClass = sample.GroupClass.Replace ("Firefox", "Web Browser");
			sample.GroupClass = sample.GroupClass.Replace ("Gecko", "Web Browser");
			sample.GroupClass = sample.GroupClass.Replace ("Desktop_window", "File Manager");
			sample.GroupClass = sample.GroupClass.Replace ("Gnome-terminal", "Terminal");
			sample.GroupClass = sample.GroupClass.Replace ("Xchat", "X-Chat");
			sample.GroupClass = sample.GroupClass.Replace ("Evolution-1.5", "Evolution");
			sample.GroupClass = sample.GroupClass.Replace ("Evolution-2.0", "Evolution");
			sample.GroupClass = sample.GroupClass.Replace ("Soffice.bin", "OpenOffice");

			return true;
		}

		private static void PositionPieWin ()
		{
			int display_width, display_height;
			Drawable d = (Display.Default.GetScreen (0)).RootWindow;
			d.GetSize (out display_width, out display_height);

			int x, y, width, height, depth;
			button.GdkWindow.GetGeometry (out x, out y, out width, out height, out depth);
			button.GdkWindow.GetPosition (out x, out y);

			if ( ( (y * 100) / display_height) < 20)
				y += height;
			else
				y -= (height + pie_height);

			if (x + pie_width > display_width)
				x = display_width - pie_width;
			if (y + pie_height > display_height)
				y = display_height - pie_height;

			piewin.Move (x, y);
		}

		private static void PopulatePieChart ()
		{
			chart.ClearSlices ();
			
			foreach (AppTime at in journal.AppTimeTable.Values) {

				double percentage = (double) (at.ElapsedTime) / (double) journal.TotalElapsed;

				if (percentage > 0.10) {
					double explode = 0.0;

					if (at.Application == last_sample.GroupClass)
						explode = 0.02;

					chart.AddSlice (percentage, at.Application, at.Color, explode);
				}
			}
		}

		private static void HandleButtonToggled (object sender, EventArgs args)
		{
			if (piewin != null) {
				piewin.Destroy ();
				piewin = null;
				return;
			}
				
			piewin = new Gtk.Window ("Timeline");
			piewin.Decorated       = false;
			piewin.DefaultHeight   = pie_height;
			piewin.DefaultWidth    = pie_width;
			piewin.SkipPagerHint   = true;
			piewin.SkipTaskbarHint = true;
			piewin.CanFocus        = false;
			piewin.TypeHint        = Gdk.WindowTypeHint.Dock;
			piewin.Stick ();

			Gtk.Frame frame = new Gtk.Frame ("Application Usage");
			
			chart = new PieChart ();
			PopulatePieChart ();

			PositionPieWin ();

			Box vbox = new Gtk.VBox (false, 2);

			vbox.PackStart (frame, true, true, 0);

			Gtk.Table table = new Gtk.Table (3, 2, true);
			// vbox.PackStart (table, false, false, 0);

			RadioButton radio1 = new RadioButton ("Default");
			RadioButton radio2 = new RadioButton ("Default");
			RadioButton radio3 = new RadioButton ("Default");
			RadioButton radio4 = new RadioButton ("Default");
			table.Attach (radio1, 0, 1, 0, 1);
			table.Attach (radio2, 1, 2, 0, 1);
			table.Attach (radio3, 0, 1, 1, 2);
			table.Attach (radio4, 1, 2, 1, 2);
				
			piewin.Add (vbox);
			frame.Add (chart);
			piewin.ShowAll ();
		}

		public static void ShowTrayMenu ()
		{
			Menu menu = new Menu ();

			MenuItem prefs_item = new MenuItem ("Preferences...");
			menu.Append (prefs_item);

			prefs_item.Activated += new EventHandler (PrefsActivated);

			MenuItem quit_item = new MenuItem ("_Remove from Panel...");
			quit_item.Activated += new EventHandler (QuitTimeline);
			menu.Append (quit_item);

			menu.ShowAll ();
		}

		static void PrefsActivated (object o, EventArgs args)
		{
			Console.WriteLine ("Prefs activated...");
		}

		static void QuitTimeline (object o, EventArgs args)
		{
			Application.Quit ();
		}

		static bool StartTrackingFocus ()
		{
			tracker = new FocusTracker ();

			tracker.FocusChanged +=
				new FocusTracker.FocusChangedHandler (HandleFocusChanged);
			
			return false;
		}

		public static void Main (string[] args)
		{
			Gnome.Program program =
				new Gnome.Program ("Timeline", "0.0", Modules.UI, args);

			icon = new NotificationArea ("Timeline");

			button = new Gtk.ToggleButton ("Timeline...");
			button.Relief = Gtk.ReliefStyle.None;
			button.Toggled += new EventHandler (HandleButtonToggled);

			icon.Add (button);
			icon.ShowAll ();

			journal = new Journal ();

			GLib.Idle.Add (new GLib.IdleHandler (StartTrackingFocus));

			program.Run ();
		}
	}
}
