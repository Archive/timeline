//
// Written using a keyboard by Nat Friedman <nat@novell.com>
//
// Copyright (C) 2004 Novell, Inc.
//

using System;
using System.Collections;
using System.Collections.Specialized;

namespace Timeline {

	class FindDistractorsMain {
		
		static void Main (string[] args)
		{
			Journal journal = new Journal ("/home/nat/.timeline/journal.csv");

			IDictionary distractors = new ListDictionary ();

			string last_app = "foo";
			for (int i = 0 ; i < journal.Activities.Count ; i ++) {
				Activity a = (Activity) journal.Activities [i];

				if (a.ElapsedTime > 20)
					continue;

				if (a.Application == last_app)
					continue;
				last_app = a.Application;

				int count;
				object distraction_count = distractors [a.Application];
				if (distraction_count == null) {
					distractors [a.Application] = new int ();
					count = 0;
				} else
					count = (int) distraction_count;

				distractors [a.Application] = count + 1;
			}

			foreach (string app in distractors.Keys)
				Console.WriteLine ("{0}: {1}", (int) distractors [app], app);
		}
	}
}
