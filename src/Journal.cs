//
// Journal.cs
//
// Copyright (C) 2004 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;
using System.Collections.Specialized;
using System.IO;

namespace Timeline {

	public class AppTime : IComparable {

		public string Application;
		public string Color;

		private int elapsed;
		private int hours;
		private int minutes;
		private int seconds;

		public string PrettyTime ()
		{
			string text = "";

			if (this.Hours == 1)
				text += "1 hour ";
			else if (this.Hours > 1)
				text += String.Format ("{0} hours ", this.Hours);

			if (this.Minutes == 1)
				text += "1 minute ";
			else if (this.Minutes > 1)
				text += String.Format ("{0} minutes ", this.Minutes);

			if (this.Minutes > 0 || this.Hours > 0)
				text += "and ";

			if (this.Seconds == 1)
				text += "1 second";
			else
				text += String.Format ("{0} seconds", this.Seconds);

			return text;
		}

		public int CompareTo (Object t)
		{
			AppTime that = (AppTime) t;

			if (this.ElapsedTime == that.ElapsedTime)
				return 0;

			if (this.ElapsedTime > that.ElapsedTime)
				return -1;

			return 1;
		}

		public int ElapsedTime {
			get { return this.elapsed; }

			set {
				this.elapsed = value;
				this.hours   = this.elapsed / 3600;
				this.seconds = this.elapsed % 60;
				this.minutes = (this.elapsed / 60) % 60;
			}
		}

		public int Hours {
			get { return this.hours; }
		}

		public int Minutes {
			get { return this.minutes; }
		}

		public int Seconds {
			get { return this.seconds; }
		}
	}

	public class Journal {
		
		ArrayList activities = new ArrayList ();
		public Hashtable AppTimeTable = new Hashtable ();
		public int TotalElapsed;

		private string log_path;
		private StreamWriter log_sw;

		string filename;

		public void Load (string filename)
		{
			this.filename = filename;

			activities.Clear ();

			StreamReader sr = new StreamReader (filename);

			string line;
			while ((line = sr.ReadLine ()) != null) {
				Activity activity = new Activity (line);
				AddActivity (activity);
			}
		}

		private static string PickColor (AppTime at)
		{

			if (at.Application == "Web Browser")
				return "blue";
			if (at.Application == "Instant Messenger")
				return "red";
			if (at.Application == "X-Chat")
				return "red";
			if (at.Application == "Emacs")
				return "grey";
			if (at.Application == "Terminal")
				return "darkgrey";
			if (at.Application == "File Manager")
				return "green";
			if (at.Application == "Evolution")
				return "yellow";

			Random r = new Random ();

			double rd = r.NextDouble ();

			if (rd < 0.10) return "red";
			if (rd < 0.20) return "yellow";
			if (rd < 0.30) return "green";
			if (rd < 0.40) return "blue";
			if (rd < 0.50) return "grey";
			if (rd < 0.60) return "orange";
			if (rd < 0.70) return "lightblue";
			if (rd < 0.80) return "lightgreen";
			if (rd < 0.90) return "pink";
			return "blue";
		}

		public void AddActivity (Activity activity)
		{
			if (activity == null || activity.Application == null)
				return;
			
			Console.WriteLine ("Logging activity: " + activity.Application +
					   " [" + activity.Title + "]" +
					   " (" + activity.ElapsedTime + " secs)");

			activities.Add (activity);
			WriteToLog (activity);
		}

		public void WriteToLog (Activity activity)
		{
			log_sw.Write (String.Format ("\"{0}\",\"{1}\",{2},{3},{4},{5},{6}\n",
						     activity.Application,
						     activity.Title,
						     activity.ElapsedTime,
						     (activity.StartTime - new DateTime (1970, 1, 1).ToLocalTime ()).TotalSeconds,
						     (activity.EndTime - new DateTime (1970, 1, 1).ToLocalTime ()).TotalSeconds,
						     "", activity.Id));

			log_sw.Flush ();
		}

		public void UpdateAppTimes (string app, int elapsed_time)
		{
			TotalElapsed += elapsed_time;

			AppTime at = (AppTime) AppTimeTable [app];

			if (at == null) {
				at = new AppTime ();
				at.Application = app;
				at.ElapsedTime = elapsed_time;
				at.Color = PickColor (at);

				AppTimeTable [app] = at;
			}

			at.ElapsedTime += elapsed_time;
		}

		public Journal ()
		{
			log_path = Environment.GetEnvironmentVariable ("HOME");
			log_path = Path.Combine (log_path, ".timeline");
			log_path = Path.Combine (log_path, "journal-current.csv");

			try {
				Directory.CreateDirectory (Path.Combine (Environment.GetEnvironmentVariable ("HOME"), ".timeline"));
			} catch {
			}

			FileStream fs = new FileStream (log_path, System.IO.FileMode.Append);
			log_sw = new StreamWriter (fs);
		}

		public string Filename {
			get {
				return filename;
			}
		}

		public ArrayList Activities {
			get { return activities; }
		}

		public Activity GetActivityByTime (DateTime dt)
		{
			Activity activity;

			int i0 = 0;
			int i1 = activities.Count-1;
			
			while (i1 - i0 > 1) {
				int i = (i0 + i1) / 2;
				activity = (Activity) activities [i];

				if (activity.Contains (dt))
					return activity;
				else if (dt < activity.StartTime)
					i1 = i;
				else if (dt >= activity.EndTime)
					i0 = i;
			}

			activity = (Activity) activities [i0];
			if (activity.Contains (dt))
				return activity;

			activity = (Activity) activities [i1];
			if (activity.Contains (dt))
				return activity;

			return null;
		}

		public Activity FirstActivity ()
		{
			return (Activity) activities [1];
		}

		public IDictionary GroupActivitiesByApp ()
		{
			IDictionary apps;

			apps = new ListDictionary ();

			foreach (Activity activity in activities) {
				IList app = (IList) apps [activity.Application];
				if (app == null) {
					app = new ArrayList ();
					apps [activity.Application] = app;
				}

				app.Add (activity);
			}

			return apps;
		}

		public string GenerateSummary ()
		{
			IDictionary apps = GroupActivitiesByApp ();
			ArrayList apptimes = new ArrayList ();
			string html;

			int totalelapsed = 0;

			foreach (DictionaryEntry app in apps) {

				AppTime at = new AppTime ();
				at.Application = ((Activity) ((IList) app.Value)[0]).Application;
				at.ElapsedTime = 0;

				foreach (Activity activity in (IList) app.Value)
					at.ElapsedTime += activity.ElapsedTime;

				totalelapsed += at.ElapsedTime;

				apptimes.Add (at);
			}

			AppTime total = new AppTime ();
			total.Application = "Total time elapsed";
			total.ElapsedTime = totalelapsed;

			apptimes.Sort ();

			html = "<table>";

			html += String.Format ("<tr><td><b>Total time elapsed:</b></td> <td colspan=2>{0}</td></tr>", total.PrettyTime ());
			html += "<tr><td colspan=3>&nbsp;</td></tr>";

			foreach (AppTime at in apptimes) {

				html += String.Format ("<tr> <td><b>{0}</b></td>", at.Application);

				html += String.Format ("<td>{0}%</td>", at.ElapsedTime * 100 / totalelapsed);

				html += String.Format ("<td>{0}</td>", at.PrettyTime ());

				html += "</tr>";
			}

			html += "</table>";

			return html;
		}
	}

	public class JournalWatcher
	{
		Journal journal;
		long file_length;
		uint timeout_id = 0;

		public JournalWatcher (Journal journal, uint sleepseconds) {
			this.journal = journal;

			FileInfo file_info = new FileInfo (journal.Filename);
			file_length = file_info.Length;

			timeout_id = GLib.Timeout.Add (sleepseconds * 1000, 
						       new GLib.TimeoutHandler (JournalCheckChange));
		}

		~JournalWatcher () {
			if (timeout_id != 0)
				GLib.Source.Remove (timeout_id);

		}

		public delegate void ChangeHandler (JournalWatcher watcher, Journal journal);
		public event ChangeHandler ChangeEvent;

		public bool JournalCheckChange ()
		{
			FileInfo file_info = new FileInfo (journal.Filename);

			if (file_info.Length != file_length) {
				file_length = file_info.Length;

				Console.WriteLine ("Journal changed. Reloading.");

				journal.Load (journal.Filename);
				ChangeEvent (this, this.journal);
			}

			return true;
		}
	}
}
