/*
 *  Copyright 1998, Nat Friedman <nat@nat.org>
 *  Copyright 2004, Novell Inc.
 *
 * A program to print information about the X window that has focus,
 * and some other handy things.
 *
 * Created Tue Nov  3 01:06:22 1998.
 * Updated Wed Jul 21 00:45:11 2004.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <X11/Xlib.h>
#include <X11/Xutil.h>

static void
print_resource_info (Display *dpy, Window win)
{
	XClassHint classhint;
	Window class_win;

	class_win = win;

	while (1) {
		Window parent_win;
		Window root_win;
		Window *child_list;
		unsigned int num_children;
		
		if (XGetClassHint (dpy, class_win, &classhint)) {
			char *name;
			
			if (classhint.res_name)
				printf ("CLASSHINT_RES_NAME=\"%s\"\n", classhint.res_name);

			if (classhint.res_class)
				printf ("CLASSHINT_RES_CLASS=\"%s\"\n", classhint.res_class);

			if (XFetchName (dpy, class_win, &name))
				printf ("NAME=\"%s\"\n", name);

			return;
		}

		if (! XQueryTree (dpy, class_win, &root_win, &parent_win, &child_list, &num_children))
			return;

		class_win = parent_win;
		printf ("Currently: %x", class_win);
	}
}

static void
print_window (Display *dpy, Window win)
{
	char * name;
	char * icon_name;

	if (XFetchName (dpy, win, &name) == 0)
		name = NULL;

	printf ("TITLE=");
	if (name == NULL)
		printf ("\"(none)\"\n");
	else
		printf ("\"%s\"\n", name);

	printf ("WID=0x%x\n", (XID) win);

	print_resource_info (dpy, win);
	
	XGetIconName (dpy, win, &icon_name);
	if (icon_name)
		printf ("ICON=\"%s\"\n", icon_name);
} /* print_window */

int
main (int argc, char ** argv)
{
	Display * dpy;
	Window focus_win;
	int revert_to;

	dpy = XOpenDisplay (NULL);
	if (dpy == NULL) {
		fprintf (stderr, "%s: Could not open display\n", *argv);
		exit (1);
	}

	printf ("DISPLAY_WIDTH=%d\n", DisplayWidth (dpy, DefaultScreen(dpy)));
	printf ("DISPLAY_HEIGHT=%d\n", DisplayHeight (dpy, DefaultScreen(dpy)));

	XGetInputFocus (dpy, &focus_win, &revert_to);
  
	print_window (dpy, focus_win);

	return 0;
} /* main */
