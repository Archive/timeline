//
// PieChart.cs
//
// Copyright (C) 2004 Novell, Inc.
//

//
// Permission is hereby granted, free of charge, to any person obtaining a
// copy of this software and associated documentation files (the "Software"),
// to deal in the Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish, distribute, sublicense,
// and/or sell copies of the Software, and to permit persons to whom the
// Software is furnished to do so, subject to the following conditions:
//
// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
// FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
// DEALINGS IN THE SOFTWARE.
//

using System;
using System.Collections;
using System.Drawing;

namespace Timeline {

	public class PieChart : Gtk.DrawingArea {

		public PieChart ()
		{
			this.ExposeEvent += OnExposed;
			this.ButtonReleaseEvent += OnRelease;
		}

		void OnExposed (object o, Gtk.ExposeEventArgs args)
		{
			Paint ();
		}

		void OnRelease (object o, Gtk.ButtonReleaseEventArgs args)
		{
			Console.WriteLine ("X: " + args.Event.X + " Y: " + args.Event.Y);
		}


		////////////////////////////////

		private class Slice {
			private double val;
			private double offset;
			private string label;
			private string colorName;
			private Gdk.Color color;

			public double Value {
				get { return val; }
				set { 
					val = (double) value;
					if (val < 0)
						throw new Exception ("Illegal negative slice value!");
				}
			}

			public double Offset {
				get { return offset; }
				set { 
					offset = (double) value;
				}
			}

			public string Label {
				get { return label; }
				set { label = (string) value; }
			}

			public void SetColor (string _colorName)
			{
				colorName = _colorName;
				Gdk.Color.Parse (_colorName, ref color);
				Gdk.Colormap.System.AllocColor (ref color, true, true);
			}

			public Gdk.Color Color {
				get { return color; }
			}

			public string ColorName {
				get { return colorName; }
			}
		}

		ArrayList slices = new ArrayList ();
		double sliceTotal = 0.0;

		public void ClearSlices ()
		{
			slices = new ArrayList ();
			sliceTotal = 0.0;
			QueueDraw ();
		}
		
		public void AddSlice (double val, string label, string color, double offset) {
			Slice slice = new Slice ();
			slice.Value = val;
			slice.Label = label;
			slice.Offset = offset;
			slice.SetColor (color);

			slices.Add (slice);
			sliceTotal += val;
			QueueDraw ();
		}

		public double SliceTotal {
			get { return sliceTotal; }
		}

		////////////////////////////////

		private void Paint ()
		{
			Gdk.Drawable drawable = this.GdkWindow;
			Gdk.GC gc = new Gdk.GC (drawable);

			int w = this.Allocation.Width;
			int h = this.Allocation.Height;

			double cx = w / 2.0, cy = h / 2.0;

			double theta = 0, thetaRadians = 0;
			int sz = (int) (Math.Min (w, h) * 0.95);

			Gdk.Color edge = new Gdk.Color (0, 0, 0);
			Gdk.Colormap.System.AllocColor (ref edge, true, true);
			
			Pango.Layout layout = new Pango.Layout (this.PangoContext);
			layout.FontDescription = Pango.FontDescription.FromString ("Sans 9");
			
			foreach (Slice slice in slices) {
				double phi = 64 * 360.0 * slice.Value / SliceTotal;
				double phiRadians = 2 * Math.PI * phi / (64 * 360.0);

				double offR = slice.Offset * sz;
				double dx = Math.Cos (thetaRadians + phiRadians / 2);
				double dy = -Math.Sin (thetaRadians + phiRadians / 2);
				
				int th0 = (int) theta;
				int th1 = (int) phi;

				gc.RgbFgColor = slice.Color;

				drawable.DrawArc (gc, true,
						  (int) (cx - sz / 2 + offR * dx),
						  (int) (cy - sz / 2 + offR * dy),
						  sz, sz, th0, th1);
				
				gc.RgbFgColor = edge;

				drawable.DrawArc (gc, false,
						  (int) (cx - sz / 2 + offR * dx),
						  (int) (cy - sz / 2 + offR * dy),
						  sz, sz, th0, th1);

				if (slice.Label != null & slice.Label != "") {

					layout.SetText (slice.Label);

					Pango.Rectangle ink = new Pango.Rectangle ();
					Pango.Rectangle logical = new Pango.Rectangle ();
					layout.GetPixelExtents (out ink, out logical);
					
					this.GdkWindow.DrawLayout (this.Style.TextGC (Gtk.StateType.Normal),
								   (int) (cx + (sz/4 + offR) * dx) - ink.Width / 2,
								   (int) (cy + (sz/4 + offR) * dy) - ink.Height / 2,
								   layout);


				}
				
				theta += phi;
				thetaRadians += phiRadians;
			}
		}
	}
}
