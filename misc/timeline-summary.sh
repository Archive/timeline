#!/bin/sh

if [ x$1 == x--brief ]
then
    SIMPLE=true
fi

if [ x$1 == x--help ]
then
    echo "Usage: timeline-summary.sh [--brief]"
    exit 1
fi

TOTAL_SECS=$((`cat ~/.timeline/journal-current.csv |cut -d, -f 4 |sed s/$/+/g; echo 0`))
SECONDS=$(($TOTAL_SECS % 60))
MINUTES=$((($TOTAL_SECS / 60) % 60))
HOURS=$(($TOTAL_SECS / 3600))

echo -n "Total time: "
if [ $HOURS -gt 1 ] ; then echo -n "$HOURS hours, " 
elif [ $HOURS -gt 0 ] ; then echo -n "$HOURS hour, " ; fi
if [ $MINUTES -gt 1 ] ; then echo -n "$MINUTES minutes and "
elif [ $MINUTES -gt 0 ] ; then echo -n "$MINUTES minute and " ; fi
echo "$SECONDS seconds"

echo

APPLIST=`cat ~/.timeline/journal.csv  |tr -d \" |tr ' ' '_' |cut -d, -f 1 |sort -u`
for app in $APPLIST
do
    app=`echo $app |tr '_' ' '`

    if [ "x$app" != "xbeginning-default" ] ; then

        APPSECS=$((`cat ~/.timeline/journal.csv  |grep "^\"$app" |cut -d, -f 4 |sed s/$/+/g ; echo 0`))
        SECONDS=$(($APPSECS % 60))
        MINUTES=$((($APPSECS / 60) % 60))
        HOURS=$(($APPSECS / 3600))

        echo -n "$app $(($APPSECS * 100 / $TOTAL_SECS))% (" 
        if [ $HOURS -gt 0 ] ; then echo -n "$HOURS hours, " ; fi
        if [ $MINUTES -gt 0 ] ; then echo -n "$MINUTES minutes and "  ; fi
        echo "$SECONDS seconds)"

        if [ x$SIMPLE != xtrue ]
	then
            cat ~/.timeline/journal.csv  | grep "^\"$app" |cut -d, -f 2 |sort -u | (
                read TITLE
                while [ "x$TITLE" != "x" ]
                do
                    TITLESECS=$((`cat ~/.timeline/journal.csv |grep "^\"$app" |grep "\,$TITLE\," |cut -d, -f 4 |sed s/$/+/g ; echo 0`))
                    SECONDS=$(($TITLESECS % 60))
               	    MINUTES=$((($TITLESECS / 60) % 60))
                    HOURS=$(($TITLESECS / 3600))
           	    echo -n "    $TITLE $(($TITLESECS * 100 / $APPSECS))% "
         	    echo -n "(" 
        	    if [ $HOURS -gt 0 ] ; then echo -n "$HOURS hours, " ; fi
        	    if [ $MINUTES -gt 0 ] ; then echo -n "$MINUTES minutes and " ; fi
        	    echo "$SECONDS seconds)"
        	    read TITLE
                done
            )
	    echo
	fi
    fi
done
