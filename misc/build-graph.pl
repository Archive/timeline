#!/usr/bin/env perl

print "<table border=0 cellspacing=0 cellpadding=0>\n" ;
print "<style type=text/css>\n" ;
print "<!--\n" ;
print "A:link { color: #000000; text-decoration: none; }\n" ;
print "-->\n" ;
print "</style>\n" ;

print "<script>\n" ;
print "<!--\n" ;
print "function setImage (new_path) {\n" ;
print "    var elt;\n" ;
print "    elt = document.getElementById (\"the_image\");\n" ;
print "    if (elt == null) {\n" ;
print "	alert (\"Could not get the image\");\n" ;
print "	return;\n" ;
print "    }\n" ;
print "    elt.src = new_path;\n" ;
print "}\n" ;
print "-->\n" ;
print "</script>\n" ;
print "<center><img id=\"the_image\" src=trans.png width=300 height=225 border=0><p></center>\n" ;

print "<table border=0 cellspacing=1 cellpadding=0>\n" ;

sub get_color
{
  my $CLASS = $_;
  my $COLOR;

  if ($CLASS == "beginning-default") 
  {
      $COLOR="#000000" ;
  }
  if ( $CLASS == "Emacs" )
  {
      $COLOR="#ff0000" ;
    }
  if ( $CLASS == "Epiphany-bin" ) 
  {
      $COLOR="#00ff00" ;
  }
  if ( $CLASS == "Gnome-terminal" ) 
  {
      $COLOR="#0000ff" ;
  }
  if ( $IDLE == "idle" ) 
  {
      $COLOR="#ffff00" ;
  }
  return $COLOR;
}

open(JOURNAL, "$ENV{'HOME'}/.timeline/journal.csv") || die $!;
@file = ();
while (<JOURNAL>)
{
    push (@file, $_);
}
close (JOURNAL);

foreach $full_line (@file)
{
    @line = split (/,/, $full_line);
    $TOTAL_SECS = $TOTAL_SECS + $line[3];
}
$TOTAL_WIDTH=1000;

foreach $full_line (@file)
{
    if (defined ($full_line))
    {
	@line = split (/,/, $full_line);
	$IDLE = $line[2];
	$CLASS = $line[0];
	$SECS = $line[3];
	$SHOT = $line[6];
	eval { $WIDTH = $SECS * $TOTAL_WIDTH / $TOTAL_SECS };
	
	$COLOR = get_color ($CLASS);
	
	print "<td bgcolor=\"$COLOR\" width=".int ($WIDTH)." height=50 align=left><img border=0 src=\"trans.png\" width=".int ($WIDTH)." height=50 onMouseOver=\"setImage('$SHOT')\"></a></td>" ;
    }
}

print "</table>\n" ;

$IDLE="no";

print "<p><u>Legend</u><p><table border=1>\n" ;

for $full_line (@file)
{
	@line = split (/,/, $full_line);
	$CLASS = $line[0];
	if (defined ($CLASS))
	{
	    $COLOR = get_color($CLASS);
	    if ($CLASS != "beginning-default")
	    {
		print "<tr><td bgcolor=\"$COLOR\">$CLASS</td>\n" ;
	    }
	}
}


$CLASS="foo";
$IDLE="idle";
$COLOR = get_color($CLASS);
print "<tr><td bgcolor=\"$COLOR\">screensaver running</td>\n" ;

print "</table>\n" ;

$count = 0;
@UNSORTED_APPLIST = ();
for $full_line (@file)
{
    @line = split (/,/, $full_line);
    push (@UNSORTED_APPLIST, $line[0]);
}

@SORTED_APPLIST = sort {uc($a) cmp uc($b)} @UNSORTED_APPLIST;

$seen=$SORTED_APPLIST[0];
push (@APPLIST, $seen);
foreach $app (@SORTED_APPLIST)
{
    if ($app ne $seen)
    {
	push (@UNIQ_APPLIST, $app);
    }
    $seen = $app;
}

print "<p><table border=1>\n" ;
#foreach $app (@APPLIST)
# {
#  print "app : $app\n";
#
# }

foreach $app (@UNIQ_APPLIST)
{
    $APPSECS=0;
    @matched_lines = grep {/^$app/} @file;
    for $full_line (@matched_lines)
    {
	@line = split (/,/, $full_line);
	$APPSECS = $APPSECS + $line[3];
    }
    $hash{$app}=$APPSECS;
}

#print "done with the sorting of apps\n";

@APPLIST = ();
foreach $app (sort { $hash{$b} <=> $hash{$a}}(keys %hash))
{
    push (@APPLIST, $app);
}


foreach $app (@APPLIST)
{
    if ($app ne "beginning-default")
    {
 	$APPSECS = $hash{$app};
	print "<tr><td><b>$app</b></td><td>\n";
	@app_lines = grep {/^$app/} @file;
	for $app_line (@app_lines)
	{
	    @fields = split (/,/, $app_line);
	    push (@all_titles, $fields[1]);
	}
	$seen = $all_titles[0];
	@titles = ();
	for $title (sort @all_titles)
	{
	    if ($title ne $seen)
	    {
		push (@titles, $title);
	    }
	    $seen = $title;
	}
	
	for $TITLE (@titles)
	{
	    if (defined ($TITLE))
	    {
		$TITLE =~ s/\+/ /g;
		$TITLE =~ s/(\*)/( )/g;
		@title_lines = grep {/,$TITLE,/} @app_lines;
		for $title_line (@title_lines)
		{
		    @fields = split (/,/,$title_line);
		    $TITLESECS = $fields[3];
		    eval { $SECONDS = $TITLESECS % 60 };
		    eval { $MINUTES = ($TITLESECS / 60 ) % 60 };
		    eval { $HOURS = int ($TITLESECS / 3600) };
		    
		    print "$TITLE ". eval ($TITLESECS * 100 / $APPSECS)." % (";
		    if ($HOURS > 0)  { print "$HOURS hours, ";}
		    if ($MINUTES > 0) {print "$MINUTES minutes and ";}
		    print "$SECONDS seconds)<br>\n";
		}
	    }
	}	
	
	eval { $SECONDS = ($APPSECS % 60) };
	eval { $MINUTES = (($APPSECS / 60) % 60) };
	eval { $HOURS = int ($APPSECS / 3600) };
	
	print "</td><td>". eval {$APPSECS * 100 / $TOTAL_SECS }." % (";
	if ($HOURS > 0)  { print "$HOURS hours, ";}
	if ($MINUTES > 0) {print "$MINUTES minutes and ";}
	print "$SECONDS seconds)</td>\n";
    }
}
print "</table>\n" ;
