#!/bin/sh
#
# Track what the user is doing.
#
# Nat Friedman <nat@nat.org>
#
# Copyright 2004 Novell, Inc.
#

# Where to put files
OUTPUT_DIR=$HOME/.timeline

# How long before taking a screenshot after the user
# switches to an app, in secs
SHOT_DELAY=3

# Thumbnail screenshot width
SHOT_WIDTH=300

# Initialize variables
LAST_TITLE=beginning-default
LAST_CLASS=beginning-default
LAST_BLANK_STRING=active
LAST_TIMESECS=`date +%s`
SHOTTIME=$(($LAST_TIMESECS + $SHOT_DELAY))
START_TIMEHUMAN=`date +"%d %b %Y %H:%M:%S"`
LAST_CSV=beginning-default

HTML=$OUTPUT_DIR/index.html
CSV=$OUTPUT_DIR/journal.csv

# Create files/dirs
if [ ! -e $OUTPUT_DIR ]
then
    mkdir -p $OUTPUT_DIR
    if [ ! -e $OUTPUT_DIR ]; then echo "Could not create directory $OUTPUT_DIR" ; exit ; fi
fi

while true
do
        CSV=$OUTPUT_DIR/journal-`date +"%Y-%m-%d"`.csv
	if [ $CSV != $LAST_CSV ]
	then
	    rm $OUTPUT_DIR/journal-current.csv 2>&1 > /dev/null
	    ln -s $CSV $OUTPUT_DIR/journal-current.csv
	    LAST_CSV=$CSV
	fi

        TIMESTAMP=`date +"%Y-%m-%d_%H-%M-%S-%N"`
        TIMEHUMAN=`date +"%d %b %Y %H:%M:%S"`
        TIMESECS=`date +%s`

        # Grab current window information
        eval `timeline-xgetfocus`
	CLASS=$CLASSHINT_RES_CLASS

	if [ "x$TITLE" == "x(none)" ]
	then
	        TITLE=`xwininfo -children -id $WID |grep \" |cut -d\" -f 2`
	fi

	TITLE=`echo $TITLE |tr \, \;`

	# Get screensaver information
	SCREEN=`xscreensaver-command -time 2>/dev/null |grep "screen blanked"`
	if [ x"$SCREEN" != x ]
	then
	    BLANK_STRING="idle"
	    TITLE="Screensaver"
	    CLASS="Screensaver"
	    WID="0"
	else
	    BLANK_STRING="active"
	fi

	# If the user just switched between applications, set the timer and
	# take a screenshot if he stays there for a little while
	if [ x"$CLASS" != x"$LAST_CLASS" ]
	then
	        echo "Application switch: To $CLASS (\"$TITLE\") from $LAST_CLASS (\"$LAST_TITLE\") "
	        SHOTTIME=$(($TIMESECS + $SHOT_DELAY))
	fi

	if [ $TIMESECS -ge $SHOTTIME -a $SHOTTIME != 0 ]
	then
	        echo "Screenshooting..."

        	SHOT_FILE=shot-$TIMESTAMP.png
               	#SHOT_HEIGHT=$(($SHOT_WIDTH * $DISPLAY_HEIGHT / $DISPLAY_WIDTH))
               	#xwd -silent -root |convert -geometry "$SHOT_WIDTH"x"$SHOT_HEIGHT" - $OUTPUT_DIR/$SHOT_FILE
	        timeline-screenshooter --width $SHOT_WIDTH $OUTPUT_DIR/$SHOT_FILE

		SHOTTIME=0
	fi

        # Record window switching or window titlebar switching
	if [ x"$TITLE" != x"$LAST_TITLE" -o x"$BLANK_STRING" != x"$LAST_BLANK_STRING" ]
	then
	        echo "Window switch: To $CLASS (\"$TITLE\") [$BLANK_STRING] from $LAST_CLASS (\"$LAST_TITLE\") [$BLANK_STRING]"

		# Record how long the user spent in that last window
                TIMESPENT=$(($TIMESECS - $LAST_TIMESECS))
                SECONDS=$(($TIMESPENT % 60))
        	MINUTES=$(($TIMESPENT / 60))
        	HOURS=$(($TIMESPENT / 3600))

		# Update the CSV file
		echo \"$LAST_CLASS\",\"$LAST_TITLE\",$LAST_BLANK_STRING,$TIMESPENT,$LAST_TIMESECS,$TIMESECS,$LAST_SHOT_FILE,$WID >> $CSV

		# Update variables
	        START_TIMEHUMAN=$TIMEHUMAN
		LAST_TIMESECS=$TIMESECS
		LAST_TITLE=$TITLE
		LAST_CLASS=$CLASS
		LAST_BLANK_STRING=$BLANK_STRING
		LAST_SHOT_FILE=$SHOT_FILE
	fi


	sleep 2
done
