#!/bin/sh

echo "<table border=0 cellspacing=0 cellpadding=0>"
echo "<style type=text/css>"
echo "<!--"
echo "A:link { color: #000000; text-decoration: none; }"
echo "-->"
echo "</style>"

echo "<script>"
echo "<!--"
echo "function setImage (new_path) {"
echo "    var elt;"
echo "    elt = document.getElementById (\"the_image\");"
echo "    if (elt == null) {"
echo "	alert (\"Could not get the image\");"
echo "	return;"
echo "    }"
echo "    elt.src = new_path;"
echo "}"
echo "-->"
echo "</script>"
echo "<center><img id=\"the_image\" src=trans.png width=300 height=225 border=0><p></center>"

echo "<table border=0 cellspacing=1 cellpadding=0>"

function get_color ()
{
    if [ "$CLASS" == \"beginning-default\" ] ; then COLOR="#000000" ; fi
    if [ "$CLASS" == \"Emacs\" ] ; then COLOR="#ff0000" ; fi
    if [ "$CLASS" == \"Epiphany-bin\" ] ; then COLOR="#00ff00" ; fi
    if [ "$CLASS" == \"Gnome-terminal\" ] ; then COLOR="#0000ff" ; fi
    if [ $IDLE == idle ] ; then COLOR="#ffff00" ; fi
}

TOTAL_SECS=$((`cat ~/.timeline/journal.csv |cut -d, -f 4 |sed s/$/+/g; echo 0`))
TOTAL_WIDTH=1000

cat ~/.timeline/journal.csv | (
read LINE
while [ x"$LINE" != x ]
do
   IDLE=`echo $LINE |cut -d, -f 3`
   CLASS=`echo $LINE |cut -d, -f 1`
   SECS=`echo $LINE |cut -d, -f 4`
   SHOT=`echo $LINE |cut -d, -f 7`
   WIDTH=$(($SECS * $TOTAL_WIDTH / $TOTAL_SECS))

   get_color

   echo "<td bgcolor=\"$COLOR\" width=$WIDTH height=50 align=left><img border=0 src=\"trans.png\" width=$WIDTH height=50 onMouseOver=\"setImage('$SHOT')\"></a></td>"

   read LINE
done
)

echo "</table>"

IDLE=no

echo "<p><u>Legend</u><p><table border=1>"
cat ~/.timeline/journal.csv | cut -d, -f 1 |sort -u | (
read CLASS
while [ x"$CLASS" != x ]
do
   get_color

   if [ x"$CLASS" != x\"beginning-default\" ]
   then
       echo "<tr><td bgcolor=\"$COLOR\">$CLASS</td>"
   fi

   read CLASS
done
)

CLASS=foo
IDLE=idle
get_color
echo "<tr><td bgcolor=\"$COLOR\">screensaver running</td>"

echo "</table>"

APPLIST=`cat ~/.timeline/journal.csv  |tr -d \" |tr ' ' '_' |cut -d, -f 1 |sort -u`

echo "<p><table border=1>"
for app in $APPLIST
do
    app=`echo $app |tr '_' ' '`

    if [ "x$app" != "xbeginning-default" ] ; then
    APPSECS=$((`cat ~/.timeline/journal.csv  |grep "^\"$app" |cut -d, -f 4 |sed s/$/+/g ; echo 0`))
    echo "<tr><td><b>$app</b></td><td>"
    cat ~/.timeline/journal.csv  | grep "^\"$app" |cut -d, -f 2 |sort -u | (
        read TITLE
        while [ "x$TITLE" != "x" ]
        do
            TITLESECS=$((`cat ~/.timeline/journal.csv |grep "^\"$app" |grep "\,$TITLE\," |cut -d, -f 4 |sed s/$/+/g ; echo 0`))
            SECONDS=$(($TITLESECS % 60))
       	    MINUTES=$((($TITLESECS / 60) % 60))
            HOURS=$(($TITLESECS / 3600))
   	    echo -n "$TITLE $(($TITLESECS * 100 / $APPSECS)) % "
 	    echo -n "(" 
	    if [ $HOURS -gt 0 ] ; then echo "$HOURS hours, " ; fi
	    if [ $MINUTES -gt 0 ] ; then echo "$MINUTES minutes and " ; fi
	    echo "$SECONDS seconds)<br>"
	    read TITLE
        done
    )

    SECONDS=$(($APPSECS % 60))
    MINUTES=$((($APPSECS / 60) % 60))
    HOURS=$(($APPSECS / 3600))

    echo -n "</td><td>$(($APPSECS * 100 / $TOTAL_SECS)) % " 

    echo -n "(" 
    if [ $HOURS -gt 0 ] ; then echo "$HOURS hours, " ; fi
    if [ $MINUTES -gt 0 ] ; then echo "$MINUTES minutes and "  ; fi
    echo "$SECONDS seconds)</td>"
    fi
done
echo "</table>"

#cat ~/.timeline/journal.csv | (
#read LINE
#while [ x"$LINE" != x ]
#do
#   IDLE=`echo $LINE |cut -d, -f 3`
#   CLASS=`echo $LINE |cut -d, -f 1`
#   SECS=`echo $LINE |cut -d, -f 4`
#   SHOT=`echo $LINE |cut -d, -f 7`
#   WIDTH=$(($SECS * $TOTAL_WIDTH / $TOTAL_SECS))
#
#   get_color
#
#   echo "<tr><td align=top><b>Application: $CLASS</b>  height=50 align=left><a href=\"$SHOT\"><img border=0 src=\"trans.png\" width=$WIDTH height=50></a></td>"
#
#   read LINE
#done
#)

