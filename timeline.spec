Summary:     Activity Monitoring Tool
Name:        timeline
Version:     0.1
Release:     1
License:     GPL
Group:       Applications/System
Source:      timeline-%{version}.tar.gz
BuildRoot:   /var/tmp/%{name}-root
BuildPrereq: libgnomeui-devel, gtk2-devel
Requires:    libgnomeui >= 2.2, gtk2 >= 2.4, gconf2 >= 2.6

%description
A tool to monitor and report what you do with your computer all day.

%prep
%setup -q

%build
./configure --prefix=%{_prefix} \
	--localstatedir=/var/lib \
	--datadir=%{_prefix}/share
make

%install
rm -rf $RPM_BUILD_ROOT
%makeinstall

%clean
rm -rf $RPM_BUILD_ROOT

%post

%files
%defattr(-,root,root)
%doc COPYING README
%{_prefix}/lib/libtrayicon*
%{_prefix}/lib/TrayIcon.dll
%{_prefix}/bin/xgetfocus
%{_prefix}/bin/timeline/
%{_prefix}/lib/timeline/*

%changelog
* Sat Aug 21 2004 Nat Friedman <nat@novell.com>
- initial packaging of 0.1
